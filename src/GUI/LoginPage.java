package GUI;

import javax.swing.*;
import java.awt.*;

import function.*;

public class LoginPage extends JFrame {
    private LoginForm loginForm = new LoginForm();
    private RefundTicket refundTicket = new RefundTicket();
    private AdminsPanel adminsPanel = new AdminsPanel();
    private FlightManagement flightManagement = new FlightManagement();
    private GUI.FAManagement FAManagement=new FAManagement();
    private PilotManagement pilotManagement=new PilotManagement();
    private CheckTheTicket checkTicket = new CheckTheTicket();
    private PlaneManagement planeManagement= new PlaneManagement();
    static private CustomersPanel customersPanel = new CustomersPanel();
    static private SignUpPage signUpPage = new SignUpPage();
    static private BuyTicket buyTicketPage = new BuyTicket();
    static private CardLayout card;
    static private Container c;
    static private Passenger passenger;

    LoginPage() {
        new CheckTheTicket();

        c = getContentPane();
        card = new CardLayout();
        c.setLayout(card);
        c.add("login", loginForm);
        c.add("CustomerPanel", customersPanel);
        c.add("signUp", signUpPage);
        c.add("buyTicket", buyTicketPage);
        c.add("refundTicket", refundTicket);
        c.add("adminPanel", adminsPanel);
        c.add("checkTicket", checkTicket);
        c.add("planeManagement", new JScrollPane(planeManagement,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER));
        c.add("flightManagement", new JScrollPane(flightManagement,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER));
        c.add("flightAttendant", new JScrollPane(FAManagement,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER));
        c.add("pilot", new JScrollPane(pilotManagement,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER));
        ImageIcon logo = new ImageIcon("src/GUI/images/logo.jpg");
        this.setIconImage(logo.getImage());
        this.setTitle("Airport Manager");

    }

    public static void setCard(String s) {
        if (s.equals("buyTicket")) {
            buyTicketPage.update();
        } else if (s.equals("signUp")) {
            signUpPage.Default();
        }
        else if(s.equals("CustomerPanel")){
            customersPanel.update();
        }
        card.show(c, s);
    }

    public static void setPassenger(String... s) {
        try {
            passenger = new Passenger(s[0], s[1], s[2], s[3], s[4]
                    , String.valueOf(signUpPage.password.getPassword()));
        }catch(Exception e){
            JOptionPane.showMessageDialog(new JPanel()
                    , "There was an error while signing up" +
                            " \n Please try later!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }
    public static void setPassenger(Passenger p){
        passenger=p;
    }

    public static Passenger getPassenger() {
        return passenger;
    }

    public static void main(String[] args) {
        LoginPage cl = new LoginPage();
        cl.setResizable(false);
        cl.setSize(1280, 765);
        cl.setVisible(true);
        cl.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
