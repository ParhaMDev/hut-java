package GUI;

import function.Passenger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Objects;

import static GUI.LoginPage.*;

public class CustomersPanel extends JLabel implements ActionListener {

    private JButton buyTicket = new JButton()
            , ticketRefund = new JButton()
            , checkingTicket = new JButton()
            , logOut = new JButton("log out")
            , deleteAccount = new JButton("Delete my account")
            , edit = new JButton("Click to edit !")
            , cancel = new JButton("Cancel");

    private JLabel[] infoL = {new JLabel("first name :")
            , new JLabel("last name :")
            , new JLabel("father's name :")
            , new JLabel("birthdate :")
            , new JLabel("national id :")
            , new JLabel("password :")};

    private JLabel profile = new JLabel();

    private String[] data = new String[6];

    private JTextField[] info = {new JTextField()
            , new JTextField()
            , new JTextField()
            , new JTextField()
            , new JTextField()
            , new JTextField()};


    CustomersPanel() {

        ImageIcon background = new ImageIcon("src/GUI/images/CustomersPage.jpg");


        JLabel bgColor = new JLabel();
        bgColor.setBackground(new Color(0XC1E1F6));
        bgColor.setBounds(900, 0, 380, 750);
        bgColor.setOpaque(true);


        JLabel text = new JLabel("Customers Panel");
        text.setBounds(967, 10, 300, 100);
        text.setForeground(new Color(0X1F63AA));
        text.setFont(new Font("Sans", Font.BOLD, 28));


        JLabel bg = new JLabel();
        bg.setIcon(background);
        bg.setBounds(0, 0, 1280, 728);


        buyTicket.setFocusable(false);
        buyTicket.setBackground(new Color(0XC3D4EE));
        buyTicket.setBounds(930, 490, 67, 50);
        buyTicket.setBackground(new Color(0XC1E1F6));
        buyTicket.setBorder(null);
        ImageIcon ticketIcon = new ImageIcon("src/GUI/images/ticket.png");
        buyTicket.setIcon(ticketIcon);
        buyTicket.addActionListener(this);


        ticketRefund.setFocusable(false);
        ticketRefund.setBackground(new Color(0XC1E1F6));
        ticketRefund.setBounds(1030, 500, 54, 40);
        ticketRefund.setBorder(null);
        ImageIcon refund = new ImageIcon("src/GUI/images/refund.png");
        ticketRefund.setIcon(refund);
        ticketRefund.addActionListener(this);


        checkingTicket.setFocusable(false);
        checkingTicket.setBackground(new Color(0XC1E1F6));
        checkingTicket.setBounds(1130, 500, 70, 32);
        checkingTicket.setBorder(null);
        ImageIcon check = new ImageIcon("src/GUI/images/check.png");
        checkingTicket.setIcon(check);
        checkingTicket.addActionListener(this);


        edit.setFocusable(false);
        edit.setForeground(new Color(0X1F63AA));
        edit.setBackground(new Color(0XC1E1F6));
        edit.setBounds(925, 230, 100, 30);
        edit.setBorder(null);


        cancel.setFocusable(false);
        cancel.setForeground(Color.red);
        cancel.setBackground(new Color(0XC1E1F6));
        cancel.setBounds(1130, 230, 50, 30);
        cancel.setBorder(null);
        cancel.setVisible(false);


        profile.setFocusable(false);
        profile.setBackground(new Color(0XC1E1F6));
        profile.setBounds(1010, 120, 130, 130);
        profile.setBorder(null);
        ImageIcon profilePic = new ImageIcon("src/GUI/images/profile.png");
        profile.setIcon(profilePic);


        logOut.setForeground(Color.white);
        logOut.setFont(new Font("Arial", Font.PLAIN, 18));
        logOut.setBackground(new Color(0x1F63AA));
        logOut.setBounds(20, 20, 90, 40);
        logOut.setFocusable(false);
        logOut.addActionListener(this);


        deleteAccount.setForeground(Color.red);
        deleteAccount.setFont(new Font("Arial", Font.PLAIN, 18));
        deleteAccount.setBackground(new Color(0XC1E1F6));
        deleteAccount.setBounds(980, 630, 200, 40);
        deleteAccount.setBorder(null);
        deleteAccount.setFocusable(false);
        deleteAccount.addActionListener(this);


        for (int i = 0, y = 0; i < info.length; i++, y += 35) {

            info[i].setBounds(1050, 260 + y, 200, 30);
            info[i].setFont(new Font("Sans", Font.PLAIN, 15));
            info[i].setBackground(new Color(0XC1E1F6));
            info[i].setForeground(Color.black);
            info[i].setBorder(null);
            info[i].setEditable(false);
            this.add(info[i]);

        }


        for (int i = 0, y = 0; i < infoL.length; i++, y += 35) {

            infoL[i].setBounds(920, 260 + y, 200, 30);
            infoL[i].setFont(new Font("Sans", Font.PLAIN, 15));
            infoL[i].setBackground(new Color(0xC1E1F6));
            infoL[i].setForeground(new Color(0X1F63AA));
            infoL[i].setBorder(null);
            this.add(infoL[i]);

        }


        setLayout(null);
        setSize(1000, 728);
        add(text);
        add(buyTicket);
        add(ticketRefund);
        add(checkingTicket);
        add(edit);
        add(profile);
        add(cancel);
        add(logOut);
        add(deleteAccount);
        add(bgColor);
        add(bg);


        edit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                switch (edit.getText()) {

                    case "Click to edit !": {
                        for (int i = 0; i < info.length; i++) {
                            info[i].setEditable(true);
                        }
                        cancel.setVisible(true);
                        edit.setText("Click to save !");
                        edit.setForeground(Color.red);
                        break;
                    }

                    case "Click to save !": {
                        try {
                            LoginPage.getPassenger().edit(Passenger.Change.FIRST_NAME, info[0].getText());
                            LoginPage.getPassenger().edit(Passenger.Change.LAST_NAME, info[1].getText());
                            LoginPage.getPassenger().edit(Passenger.Change.FATHER_NAME, info[2].getText());
                            LoginPage.getPassenger().edit(Passenger.Change.BIRTHDATE, info[3].getText());
                            LoginPage.getPassenger().edit(Passenger.Change.NATIONAL_ID, info[4].getText());
                            LoginPage.getPassenger().edit(Passenger.Change.PASSWORD, info[5].getText());
                        } catch (Exception a) {
                            JOptionPane.showMessageDialog(new JPanel()
                                    , "There was an error while editing!\n Please try later!"
                                    , "Error!", JOptionPane.ERROR_MESSAGE);
                        }
                        for (int i = 0; i < info.length; i++) {
                            info[i].setEditable(false);
                        }
                        cancel.setVisible(false);
                        edit.setText("Click to edit !");
                        edit.setForeground(new Color(0X1F63AA));
                        update();
                        break;
                    }
                }
            }
        });


        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                cancel.setVisible(false);

                for (int i = 0; i < info.length; i++) {
                    info[i].setEditable(false);
                }

                edit.setText("Click to edit !");
                edit.setForeground(new Color(0X1F63AA));
                update();
            }
        });


        deleteAccount.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                deleteAccount.setFont(new Font("Arial", Font.PLAIN, 20));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                deleteAccount.setFont(new Font("Arial", Font.PLAIN, 18));
            }
        });


    }


    void update() {
        data[0] = LoginPage.getPassenger().getFirstName();
        data[1] = LoginPage.getPassenger().getLastName();
        data[2] = LoginPage.getPassenger().getFatherName();
        data[3] = LoginPage.getPassenger().getBirthdate();
        data[4] = LoginPage.getPassenger().getNationalId();
        data[5] = LoginPage.getPassenger().getPassword();
        for (int i = 0; i < info.length; i++) {
            info[i].setText(data[i]);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (Objects.equals(deleteAccount, e.getSource())) {
            int reply = JOptionPane.showConfirmDialog(null
                    , "Are you sure you want to delete your account?\n We will not be able th bring it back"
                    , "Delete?", JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_OPTION) {
                try {
                    LoginPage.getPassenger().delete();
                    LoginPage.setCard("login");
                } catch (Exception a) {
                    JOptionPane.showMessageDialog(new JPanel()
                            , "Could not delete the account Something went wrong! \n Please try again"
                            , "Error!", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        if (Objects.equals(buyTicket, e.getSource())) {
            LoginPage.setCard("buyTicket");
        }

        if (Objects.equals(ticketRefund, e.getSource())) {
            LoginPage.setCard("refundTicket");
        }

        if (Objects.equals(logOut, e.getSource())) {
            LoginPage.setCard("login");
        }

        if (Objects.equals(checkingTicket, e.getSource())) {
            LoginPage.setCard("checkTicket");
        }
    }
}

