package GUI;


import function.Ticket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

import static GUI.LoginPage.*;


public class RefundTicket extends JLabel implements ActionListener {

    JTextField serialNumber = new JTextField();
    JButton button = new JButton("DONE!");
    JButton back = new JButton("<< BACK");

    RefundTicket() {

        ImageIcon bg = new ImageIcon("src/GUI/images/terminal.jpg");


        JLabel title = new JLabel("Refund Ticket");
        title.setBounds(990, 5, 250, 100);
        title.setFont(new Font("arial", Font.PLAIN, 35));
        title.setForeground(new Color(0XF0F7FD));


        serialNumber.setBounds(980, 370, 200, 35);
        serialNumber.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        serialNumber.setFont(new Font("Sans-Pro", Font.PLAIN, 16));
        serialNumber.setBackground(new Color(0XF0F7FD));


        JLabel serialNumberLabel = new JLabel("Serial Number :");
        serialNumberLabel.setBounds(980, 300, 200, 100);
        serialNumberLabel.setFont(new Font("Arial", Font.BOLD, 14));
        serialNumberLabel.setForeground(new Color(0xF0F7FD));


        JLabel color = new JLabel();
        color.setBounds(900, 0, 380, 750);
        color.setBackground(new Color(0x72355D9B, true));
        color.setOpaque(true);


        button.setBounds(1010, 590, 140, 35);
        button.setFocusable(false);
        button.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        button.setFont(new Font("Arial", Font.PLAIN, 20));
        button.setBackground(new Color(0XF0F7FD));
        button.addActionListener(this);

        back.setBounds(50, 30, 140, 35);
        back.setFocusable(false);
        back.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        back.setFont(new Font("Arial", Font.PLAIN, 20));
        back.setBackground(new Color(0XF0F7FD));
        back.addActionListener(this);

        setIcon(bg);
        setLayout(null);
        setSize(1280, 728);
        add(title);
        add(serialNumber);
        add(serialNumberLabel);
        add(button);
        add(back);
        add(color);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Objects.equals(button, e.getSource())) {
            if (serialNumber.getText().isBlank()) {
                JOptionPane.showMessageDialog(new JPanel()
                        , "please enter the serial number!"
                        , "Error!", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    Ticket t = new Ticket(LoginPage.getPassenger(), serialNumber.getText());
                    int reply = JOptionPane.showConfirmDialog(null,
                            "Are you sure?\n flight date : "
                                    + t.getFlight().getFlightDate() + "\n flight time : "
                                    + t.getFlight().getFlightTime() + "\n from : "
                                    + t.getFlight().getOrigin() + "\n to : "
                                    + t.getFlight().getDestination() + "\n price : "
                                    + t.getFlight().getPrice() + "\n serial number : "
                                    + t.getSerialNumber(), "Refund?", JOptionPane.YES_NO_OPTION);

                    if (reply == JOptionPane.YES_OPTION) {
                        try {
                            t.delete();
                            LoginPage.setCard("CustomerPanel");
                        } catch (Exception a) {
                            JOptionPane.showMessageDialog(new JPanel()
                                    , "Could not refund the ticket Something went wrong!" +
                                            " \n Please try again", "Error!", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } catch (Exception a) {
                    JOptionPane.showMessageDialog(new JPanel()
                            , "Could not find the ticket Something went wrong!" +
                                    " \n Please try again", "Error!", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            LoginPage.setCard("CustomerPanel");
        }
    }
}
