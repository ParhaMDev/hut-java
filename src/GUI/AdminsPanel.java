package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;
import java.util.Optional;

import static GUI.LoginPage.*;

public class AdminsPanel extends JLabel implements ActionListener {
    JButton flightsButton = new JButton("Flights Management"),
            pilotButton = new JButton("Pilots Management"),
            planeButton = new JButton("Plane Management"),
            attendantButton = new JButton("Flight Attendants Management"),
            exitButton = new JButton("EXIT");

    AdminsPanel() {
        ImageIcon bg = new ImageIcon("src/GUI/images/img.jpg");

        flightsButton.setBounds(920, 120, 260, 40);
        flightsButton.setFocusable(false);
        flightsButton.setFont(new Font("Arial", Font.PLAIN, 18));
        flightsButton.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        flightsButton.setBackground(new Color(0Xe2ecff));
        flightsButton.addActionListener(this);


        pilotButton.setBounds(920, 200, 260, 40);
        pilotButton.setFocusable(false);
        pilotButton.setFont(new Font("Arial", Font.PLAIN, 18));
        pilotButton.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        pilotButton.setBackground(new Color(0Xe2ecff));
        pilotButton.addActionListener(this);


        attendantButton.setBounds(920, 280, 260, 40);
        attendantButton.setFocusable(false);
        attendantButton.setFont(new Font("Arial", Font.PLAIN, 18));
        attendantButton.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        attendantButton.setBackground(new Color(0Xe2ecff));
        attendantButton.addActionListener(this);


        planeButton.setBounds(920, 360, 260, 40);
        planeButton.setFocusable(false);
        planeButton.setFont(new Font("Arial", Font.PLAIN, 18));
        planeButton.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        planeButton.setBackground(new Color(0Xe2ecff));
        planeButton.addActionListener(this);


        exitButton.setBounds(920, 440, 260, 40);
        exitButton.setFocusable(false);
        exitButton.setFont(new Font("Oswald", Font.PLAIN, 18));
        exitButton.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        exitButton.setBackground(new Color(0Xe2ecff));
        exitButton.addActionListener(this);


        setIcon(bg);
        setSize(1280, 750);
        add(flightsButton);
        add(pilotButton);
        add(attendantButton);
        add(planeButton);
        add(exitButton);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Objects.equals(flightsButton, e.getSource())) {
            LoginPage.setCard("flightManagement");
        }
        else if (Objects.equals(attendantButton, e.getSource())) {
            LoginPage.setCard("flightAttendant");
        }
        else if (Objects.equals(pilotButton, e.getSource())) {
            LoginPage.setCard("pilot");
        }
        else if (Objects.equals(planeButton, e.getSource())) {
            LoginPage.setCard("planeManagement");
        }
        else if (Objects.equals(exitButton, e.getSource())) {
            LoginPage.setCard("login");
        }
    }
}
