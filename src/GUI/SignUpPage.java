package GUI;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Objects;

import static GUI.LoginPage.*;
import static function.Person.exists;

public class SignUpPage extends JLabel implements ActionListener {

    JTextField[] fields = new JTextField[5];
    JButton next, back;
    JPasswordField password;
    JPasswordField confirm;
    JLabel[] labels = new JLabel[5];
    final String[] defaultLabel = {"first name", "last name", "father's name", "birthdate", "national id"};
    final String[] Default = {"e.g : Jimmy", "e.g : Smith", "e.g : John", "e.g : 32/3/10", "e.g J75564339"};

    SignUpPage() {
        ImageIcon background = new ImageIcon("src/GUI/images/sign in.jpg");

        password = new JPasswordField();
        confirm = new JPasswordField();
        password.setBounds(170, 470, 150, 23);
        confirm.setBounds(170, 530, 150, 23);
        password.setBackground(new Color(0xcee1ff));
        confirm.setBackground(new Color(0xcee1ff));
        password.setBorder(null);
        confirm.setBorder(null);

        JLabel passwordLabel = new JLabel("Password :");
        JLabel confirmP = new JLabel("Confirm :");
        passwordLabel.setBounds(78, 470, 80, 23);
        confirmP.setBounds(95, 530, 80, 23);
        passwordLabel.setForeground(new Color(0X5F95D3));
        confirmP.setForeground(new Color(0X5F95D3));
        passwordLabel.setFont(new Font("sans", Font.PLAIN, 16));
        confirmP.setFont(new Font("sans", Font.PLAIN, 16));

        JLabel text = new JLabel("Sign up!");
        text.setBounds(140, 10, 300, 100);
        text.setForeground(new Color(0X1F63AA));
        text.setFont(new Font("Sans", Font.BOLD, 28));

        JLabel text2 = new JLabel("Set your password");
        text2.setBounds(125, 420, 300, 30);
        text2.setForeground(new Color(0X1F63AA));
        text2.setFont(new Font("Sans", Font.BOLD, 15));

        next = new JButton("NEXT >>");
        next.setBackground(new Color(0x0F59D2));
        next.setForeground(new Color(0xcee1ff));
        next.setBounds(220, 600, 85, 30);
        next.addActionListener(this);

        back = new JButton("<< BACK");
        back.setBackground(new Color(0x0F59D2));
        back.setForeground(new Color(0xcee1ff));
        back.setBounds(60, 600, 85, 30);
        back.addActionListener(this);

        for (int i = 0, y = 0; i < 5; i++, y += 60) {
            fields[i] = new JTextField();
            fields[i].setBounds(170, 130 + y, 150, 23);
            fields[i].setBackground(new Color(0xcee1ff));
            labels[i] = new JLabel(defaultLabel[i]);
            labels[i].setBounds(50, 130 + y, 150, 23);
            labels[i].setForeground(new Color(0X5F95D3));
            labels[i].setFont(new Font("sans", Font.PLAIN, 16));

            this.add(fields[i]);
            this.add(labels[i]);
        }

        Default();

        JLabel bg = new JLabel();
        bg.setIcon(background);
        bg.setBounds(340, 0, 1000, 728);
        bg.setBackground(Color.white);

        setOpaque(true);
        setBackground(Color.white);
        add(bg);
        setLayout(null);
        setSize(1280, 728);
        add(password);
        add(confirm);
        add(passwordLabel);
        add(confirmP);
        add(next);
        add(back);
        add(text);
        add(text2);

        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for (int i = 0; i < 5; i++) {
                    Default(i);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });


        for(int i=0 ; i<fields.length ; i++){
            int finalI=i;
            fields[i].addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (fields[finalI].getText().equals(Default[finalI])) {
                        fields[finalI].setText("");
                        fields[finalI].setEditable(true);
                    }
                    Default(finalI);
                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                }

                @Override
                public void mouseExited(MouseEvent e) {
                }
            });
        }

    }

    void Default(int a) {
        for (int i = 0; i < 5; i++) {
            if (fields[i].getText().isBlank() && i != a) {
                fields[i].setText(Default[i]);
                fields[i].setEditable(false);
            }
        }
    }

    void Default() {
        for (int i = 0; i < 5; i++) {
            fields[i].setText(Default[i]);
            fields[i].setEditable(false);
        }
    }

    boolean isEmpty() {
        String message = "";

        if (String.valueOf(password.getPassword()).isBlank()) {
            message += "password : empty\n";
        } else if (String.valueOf(confirm.getPassword()).isBlank()) {
            message += "confirm your password!!\n";
        } else if (!String.valueOf(password.getPassword()).equals(String.valueOf(confirm.getPassword()))) {
            message += "password does not match!\n";
        }
        for (int i = 0; i < 5; i++) {
            Default(i);
            if (fields[i].getText().equals(Default[i]) || fields[i].getText().isBlank()) {
                message += labels[i].getText() + ": empty \n";
            }
        }

        if (!message.isBlank()) {
            JOptionPane.showMessageDialog(new JPanel(), message, "Error!", JOptionPane.ERROR_MESSAGE);
            return true;
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (Objects.equals(next, e.getSource())) {
            if (!isEmpty()) {
                switch (exists(String.valueOf(password.getPassword()), fields[4].getText(), false)) {
                    case 0: {
                        LoginPage.setPassenger(fields[0].getText()
                                , fields[1].getText(), fields[2].getText()
                                , fields[3].getText(), fields[4].getText());
                        LoginPage.getPassenger();
                        LoginPage.setCard("CustomerPanel");
                        break;
                    }
                    case 3: {
                        JOptionPane.showMessageDialog(new JPanel()
                                , "Password is not unique! \n Please try again"
                                , "Error!", JOptionPane.ERROR_MESSAGE);
                        break;
                    }
                    case 4: {
                        JOptionPane.showMessageDialog(new JPanel()
                                , "Please check you national id \n it's already used by someone else!"
                                , "Error!", JOptionPane.ERROR_MESSAGE);
                        break;
                    }
                }
            }
        }

        if (Objects.equals(back, e.getSource())) {
            setCard("login");
        }
    }
}
