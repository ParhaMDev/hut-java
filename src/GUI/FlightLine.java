package GUI;

import function.Flight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class FlightLine extends JLabel{
    JButton delete = new JButton("Delete"), edit = new JButton("Edit");
    private JLabel pilot = new JLabel(), flightNumber = new JLabel(), origin = new JLabel(), destination = new JLabel();
    private Flight flight;

    FlightLine(Flight f) {
        setSize(1180, 50);
        setBackground(new Color(0x9BD2F8));
        setBorder(BorderFactory.createEtchedBorder());
        setOpaque(true);

        flight = f;

        delete.setBounds(1100, 15, 60, 20);
        delete.setFont(new Font("Sans", Font.PLAIN, 20));
        delete.setBackground(new Color(0x9BD2F8));
        delete.setBorder(BorderFactory.createEmptyBorder());
        delete.setFocusable(false);

        edit.setBounds(1030, 15, 40, 20);
        edit.setFont(new Font("Sans", Font.PLAIN, 20));
        edit.setBackground(new Color(0x9BD2F8));
        edit.setBorder(BorderFactory.createEmptyBorder());
        edit.setFocusable(false);

        flightNumber.setText(" flight number : " + flight.getFlightNum());
        flightNumber.setBounds(30, 15, 200, 20);
        flightNumber.setFont(new Font("Sans", Font.PLAIN, 20));

        origin.setText(" from : " + flight.getOrigin());
        origin.setBounds(250, 15, 255, 20);
        origin.setFont(new Font("Sans", Font.PLAIN, 20));

        destination.setText(" to : " + flight.getDestination());
        destination.setBounds(525, 15, 255, 20);
        destination.setFont(new Font("Sans", Font.PLAIN, 20));

        pilot.setText(" pilot : " + flight.getThePilot());
        pilot.setBounds(800, 15, 210, 20);
        pilot.setFont(new Font("Sans", Font.PLAIN, 20));

        add(delete);
        add(origin);
        add(destination);
        add(flightNumber);
        add(pilot);
        add(edit);

        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JTextField newPrice = new JTextField(flight.getPrice());
                JTextField newPlaneSN = new JTextField(flight.getPlaneId());
                JTextField newOrigin = new JTextField(flight.getOrigin());
                JTextField newDestination = new JTextField(flight.getDestination());
                JTextField newDate = new JTextField(flight.getFlightDate());
                JTextField newTime = new JTextField(flight.getFlightTime());
                JTextField newPilotID = new JTextField(flight.getThePilot());
                Object[] message = {"price", newPrice
                        , "plane id", newPlaneSN
                        , "from", newOrigin
                        , "to", newDestination
                        , "flight date", newDate
                        , "flights time", newTime
                        , "pilot's personal code", newPilotID};

                int option = JOptionPane.showConfirmDialog(null, message
                        , "Edit flight", JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION) {
                    if (!newPlaneSN.getText().isBlank()
                            && !newOrigin.getText().isBlank()
                            && !newDestination.getText().isBlank()
                            && !newDate.getText().isBlank()
                            && !newTime.getText().isBlank()
                            && !newPilotID.getText().isBlank()
                            && !newPrice.getText().isBlank()) {
                        try {
                            flight.edit(Flight.Change.PLANE, newPlaneSN.getText());
                            flight.edit(Flight.Change.ORIGIN, newOrigin.getText());
                            flight.edit(Flight.Change.DESTINATION, newDestination.getText());
                            flight.edit(Flight.Change.FLIGHT_DATE, newDate.getText());
                            flight.edit(Flight.Change.FLIGHT_TIME, newTime.getText());
                            flight.edit(Flight.Change.PILOT, newPilotID.getText());
                            flight.edit(Flight.Change.PRICE, newPrice.getText());

                            flightNumber.setText(" flight number : " + flight.getFlightNum());
                            origin.setText(" from : " + flight.getOrigin());
                            destination.setText(" to : " + flight.getDestination());
                            pilot.setText(" pilot : " + flight.getThePilot());

                        } catch (Exception exception) {
                            JOptionPane.showMessageDialog(null
                                    , "there was an error! please try again"
                                    , "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null
                                , "fields cannot be empty!"
                                , "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                Object[] options = {"passengers",
                        " the pilot", "flight attendants"};
                int n = JOptionPane.showOptionDialog(null,
                        "flight serial number : "
                                + flight.getFlightSerialNumber() + "\n plane serial number : "
                                + flight.getPlaneId() + "\n flight date : "
                                + flight.getFlightDate() + "\n flight time : "
                                + flight.getFlightTime() + "\n number of passengers : "
                                + flight.getNumberOfPassengers() + "\n income : "
                                + flight.getIncome() + "\n see more information : ",
                        "flight information",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.INFORMATION_MESSAGE,
                        null,     //do not use a custom Icon
                        options,  //the titles of buttons
                        null); //default button title
                switch (n) {
                    case 0: {
                        String message = flight.getPassengers();
                        JOptionPane.showMessageDialog(null, message);
                        break;
                    }

                    case 1: {
                        JOptionPane.showMessageDialog(null, flight.getAllPilotInfo());
                        break;
                    }

                    case 2: {
                        String message = flight.getFlightAttendants();
                        JOptionPane.showMessageDialog(null, message);
                        break;
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

    }

    public Flight getFlight(){
        return flight;
    }

}
