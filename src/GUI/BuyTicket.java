package GUI;

import function.All;
import function.Flight;
import function.Ticket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;


public class BuyTicket extends JLabel implements ActionListener {
    JButton back = new JButton("<< BACK");
    JButton next = new JButton("Done!");
    JLabel firstNameLabel = new JLabel(), lastnameLabel = new JLabel(), father_nameLabel = new JLabel(), nationalId = new JLabel();

    Flight flight;

    All all = new All(All.Types.FLIGHTS,true);

    JComboBox origin = new JComboBox(), destination = new JComboBox(), flightDate = new JComboBox(), flightTime = new JComboBox();

    ArrayList<String> result = new ArrayList<>();

    BuyTicket() {
        for (int i = 0; i < all.flights.size(); i++) {
            if (!result.contains(all.flights.get(i).getOrigin())) {
                result.add(all.flights.get(i).getOrigin());
                origin.addItem(all.flights.get(i).getOrigin());
            }
        }

        ImageIcon bg = new ImageIcon("src/GUI/images/bluelagoo.jpg");
        ImageIcon takeoff = new ImageIcon("src/GUI/images/takeoff.png");
        ImageIcon landing = new ImageIcon("src/GUI/images/landing.png");


        JLabel takeOffIcon = new JLabel(takeoff);
        takeOffIcon.setBounds(200, 270, 50, 50);

        JLabel landingIcon = new JLabel(landing);
        landingIcon.setBounds(760, 270, 50, 50);


        firstNameLabel.setBounds(90, 90, 300, 50);
        firstNameLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        firstNameLabel.setForeground(new Color(0XEFFBFF));


        lastnameLabel.setBounds(390, 90, 300, 50);
        lastnameLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        lastnameLabel.setForeground(new Color(0XEFFBFF));


        father_nameLabel.setBounds(690, 90, 300, 50);
        father_nameLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        father_nameLabel.setForeground(new Color(0XEFFBFF));


        nationalId.setBounds(990, 90, 300, 50);
        nationalId.setFont(new Font("Arial", Font.PLAIN, 20));
        nationalId.setForeground(new Color(0XEFFBFF));


        origin.setBounds(280, 280, 200, 30);
        ((JLabel) origin.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
        origin.setFont(new Font("Arial", Font.PLAIN, 20));
        origin.setBackground(new Color(0XEFFBFF));


        flightTime.setBounds(280, 400, 200, 30);
        ((JLabel) flightTime.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
        flightTime.setFont(new Font("Arial", Font.PLAIN, 20));
        flightTime.setBackground(new Color(0XEFFBFF));


        destination.setBounds(840, 280, 200, 30);
        ((JLabel) destination.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
        destination.setFont(new Font("Arial", Font.PLAIN, 20));
        destination.setBackground(new Color(0XEFFBFF));


        flightDate.setBounds(840, 400, 200, 30);
        ((JLabel) flightDate.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
        flightDate.setFont(new Font("Arial", Font.PLAIN, 20));
        flightDate.setBackground(new Color(0XEFFBFF));


        next.setBounds(1020, 640, 140, 35);
        next.setFocusable(false);
        next.setBorder(BorderFactory.createEmptyBorder());
        next.setFont(new Font("Arial", Font.PLAIN, 20));
        next.setBackground(new Color(0XEFFBFF));

        back.setBounds(120, 640, 140, 35);
        back.setFocusable(false);
        back.setBorder(BorderFactory.createEmptyBorder());
        back.setFont(new Font("Arial", Font.PLAIN, 20));
        back.setBackground(new Color(0XEFFBFF));


        setIcon(bg);
        setLayout(null);
        setSize(1280, 750);
        add(firstNameLabel);
        add(lastnameLabel);
        add(father_nameLabel);
        add(nationalId);
        add(origin);
        add(destination);
        add(flightDate);
        add(flightTime);
        add(takeOffIcon);
        add(landingIcon);
        add(next);
        add(back);


        origin.addActionListener(this);
        destination.addActionListener(this);
        flightDate.addActionListener(this);
        next.addActionListener(this);
        back.addActionListener(this);

    }

    void update() {
        firstNameLabel.setText("first name :" + LoginPage.getPassenger().getFirstName());
        lastnameLabel.setText("last name :" + LoginPage.getPassenger().getLastName());
        father_nameLabel.setText("father's name :" + LoginPage.getPassenger().getFatherName());
        nationalId.setText("national id :" + LoginPage.getPassenger().getNationalId());
    }

    boolean isEmpty() {
        if (origin.getSelectedIndex() == -1 || destination.getSelectedIndex() == -1
                || flightDate.getSelectedIndex() == -1 || flightTime.getSelectedIndex() == -1) {
            return true;
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Objects.equals(origin, e.getSource())) {
            flightDate.removeAllItems();
            flightTime.removeAllItems();
            String data = (String) origin.getItemAt(origin.getSelectedIndex());
            destination.removeAllItems();
            for (int i = 0; i < all.flights.size(); i++) {
                if (all.flights.get(i).getOrigin().equals(data)) {
                    if (!exists(destination, all.flights.get(i).getDestination())
                            && all.flights.get(i).getOrigin().equals(data)) {
                        destination.addItem(all.flights.get(i).getDestination());
                    }
                }
            }
        }
        if (Objects.equals(destination, e.getSource())) {
            flightDate.removeAllItems();
            flightTime.removeAllItems();
            String data = (String) destination.getItemAt(destination.getSelectedIndex());
            for (int i = 0; i < all.flights.size(); i++) {
                if (!exists(flightDate, all.flights.get(i).getFlightDate())
                        && all.flights.get(i).getDestination().equals(data)) {
                    flightDate.addItem(all.flights.get(i).getFlightDate());
                }
            }
        }
        if (Objects.equals(flightDate, e.getSource())) {
            flightTime.removeAllItems();
            String data = (String) flightDate.getItemAt(flightDate.getSelectedIndex());
            for (int i = 0; i < all.flights.size(); i++) {
                if (!exists(flightTime, all.flights.get(i).getFlightTime()) && all.flights.get(i).getFlightDate().equals(data)) {
                    flightTime.addItem(all.flights.get(i).getFlightTime());
                }
            }
        }
        if (Objects.equals(next, e.getSource())) {
            if (!isEmpty()) {
                String data = (String) flightTime.getItemAt(flightTime.getSelectedIndex());
                for (int i = 0; i < all.flights.size(); i++) {
                    if (all.flights.get(i).getFlightTime().equals(data)) {
                        flight = all.flights.get(i);
                    }
                }
                int reply = JOptionPane.showConfirmDialog(null, "Are you sure?\n"+flight.getFlightInfo(), "Buy?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    try {
                        Ticket t = new Ticket(LoginPage.getPassenger(), flight);
                        JOptionPane.showMessageDialog(null, t.getTicketInfo(), "Info", JOptionPane.INFORMATION_MESSAGE);
                        LoginPage.setCard("CustomerPanel");
                    } catch (Exception a) {
                        JOptionPane.showMessageDialog(new JPanel(), "Could not buy the ticket Something went wrong! \n Please try again", "Error!", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(new JPanel(), "Fields cannot be empty!", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (Objects.equals(back, e.getSource())) {
            LoginPage.setCard("CustomerPanel");
        }
    }

    boolean exists(JComboBox box, Object o) {
        for (int i = 0; i < box.getItemCount(); i++) {
            if (box.getItemAt(i).equals(o)) {
                return true;
            }
        }
        return false;
    }

}
