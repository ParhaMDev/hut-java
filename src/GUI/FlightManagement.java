package GUI;

import function.All;
import function.Flight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class FlightManagement extends JLabel {

    All allFlights = new All(All.Types.FLIGHTS,true);

    ArrayList<FlightLine> lines = new ArrayList<>();

    JButton back = new JButton("Back");

    JButton newFlight = new JButton("+");

    FlightManagement() {
        JTextField planeSN = new JTextField();
        JTextField origin = new JTextField();
        JTextField destination = new JTextField();
        JTextField date = new JTextField();
        JTextField time = new JTextField();
        JTextField pilotID = new JTextField();
        JTextField price = new JTextField();
        Object[] message = {"plane id", planeSN
                , "from", origin
                , "to", destination
                , "flight date", date
                , "flights time", time
                , "pilot's national id", pilotID
                , "price", price};

        newFlight.setFont(new Font("Sans", Font.BOLD, 50));
        newFlight.setBounds(1150, 0, 60, 60);
        newFlight.setBorder(null);
        newFlight.setBackground(new Color(0x2B8BBF));
        newFlight.setForeground(Color.white);
        newFlight.setFocusable(false);
        newFlight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(null
                        , message, "new flight", JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION) {
                    if (!planeSN.getText().isBlank()
                            && !origin.getText().isBlank()
                            && !destination.getText().isBlank()
                            && !date.getText().isBlank()
                            && !time.getText().isBlank()
                            && !pilotID.getText().isBlank()
                            && !price.getText().isBlank()) {
                        try {
                            new Flight(planeSN.getText(), origin.getText()
                                    , destination.getText(), date.getText()
                                    , time.getText(), pilotID.getText()
                                    , price.getText());
                            update();
                        } catch (Exception exception) {
                            JOptionPane.showMessageDialog(null
                                    , "there was an error! please try again"
                                    , "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null
                                , "fields cannot be empty!"
                                , "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        back.setBounds(50, 10, 100, 30);
        back.setFocusable(false);
        back.setFont(new Font("Oswald", Font.PLAIN, 20));
        back.setBorder(BorderFactory.createEmptyBorder());
        back.setBackground(Color.white);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginPage.setCard("adminPanel");
            }
        });

        update();

        setOpaque(true);
        setBackground(new Color(0x2B8BBF));
    }

    void update() {
        allFlights = new All(All.Types.FLIGHTS,true);

        lines.clear();

        this.removeAll();
        for (int i = 0, y = 0; i < allFlights.flights.size(); i++, y += 100) {
            lines.add(new FlightLine(allFlights.flights.get(i)));
            lines.get(i).setBounds(50, 120 + y, 1180, 50);
            this.add(lines.get(i));
            int finalI = i;
            lines.get(i).delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    int reply = JOptionPane.showConfirmDialog(null
                            , "Are you sure you want to cancel this flight?" +
                                    "\n We will not be able th bring it back", "Cancel?"
                            , JOptionPane.YES_NO_OPTION);

                    if (reply == JOptionPane.YES_OPTION) {
                        try {
                            lines.get(finalI).getFlight().delete();
                            update();
                        } catch (Exception a) {
                            JOptionPane.showMessageDialog(new JPanel()
                                    , "Could not cancel the flight Something went wrong!" +
                                            " \n Please try again", "Error!", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            });
        }
        setPreferredSize(new Dimension(1280
                , (lines.size() == 0) ? 0 : lines.get(lines.size() - 1).getY() + 100));

        add(back);
        add(newFlight);
        revalidate();
        repaint();
    }
}
