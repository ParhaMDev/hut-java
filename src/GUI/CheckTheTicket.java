package GUI;

import function.Ticket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class CheckTheTicket extends JLabel {

    JToggleButton doneButton = new JToggleButton("Done!");
    JLabel firstBackground = new JLabel(), secondBackground = new JLabel();
    ImageIcon bg = new ImageIcon("src/GUI/images/Checkticket.jpg"), ticket = new ImageIcon("src/GUI/images/ticketView.jpg");
    JButton back = new JButton("Back");

    CheckTheTicket() {
        JLabel title = new JLabel("Check The Ticket");
        JTextField serialNumber = new JTextField();
        JLabel serialNumberLabel = new JLabel("Serial Number :");
        JLabel[] info = {new JLabel("first name")
                , new JLabel("last name")
                , new JLabel("flight number")
                , new JLabel("from")
                , new JLabel("to")
                , new JLabel("date")
                , new JLabel("time")
                , new JLabel("price")
                , new JLabel("flight serial number")
                , new JLabel("ticket serial number")};

        secondBackground.setIcon(ticket);
        secondBackground.setBounds(0, 0, 1280, 750);

        info[0].setBounds(550, 285, 200, 35);

        info[1].setBounds(770, 285, 200, 35);

        info[2].setBounds(570, 315, 200, 35);

        info[3].setBounds(740, 315, 200, 35);

        info[4].setBounds(500, 345, 200, 35);

        info[5].setBounds(740, 345, 200, 35);

        info[6].setBounds(510, 370, 200, 35);

        info[7].setBounds(740, 370, 200, 35);

        info[8].setBounds(605, 400, 200, 35);

        info[9].setBounds(610, 430, 200, 35);

        for (int i = 0; i < info.length; i++) {
            info[i].setFont(new Font("Sans-Pro", Font.PLAIN, 16));
            info[i].setBackground(Color.red);
            secondBackground.add(info[i]);
        }


        title.setBounds(1010, 5, 250, 100);
        title.setFont(new Font("arial", Font.PLAIN, 28));
        title.setForeground(new Color(0XF0F7FD));


        firstBackground.setIcon(bg);
        firstBackground.setBounds(0, 0, 1280, 750);
        firstBackground.setOpaque(true);
        firstBackground.setBackground(new Color(0X9AB3EC));


        serialNumber.setBounds(1010, 370, 200, 35);
        serialNumber.setBorder(null);
        serialNumber.setFont(new Font("Sans-Pro", Font.PLAIN, 16));
        serialNumber.setBackground(new Color(0XF0F7FD));


        serialNumberLabel.setBounds(1010, 300, 200, 100);
        serialNumberLabel.setFont(new Font("Arial", Font.BOLD, 14));
        serialNumberLabel.setForeground(new Color(0xF0F7FD));


        doneButton.setBounds(1050, 670, 150, 35);
        doneButton.setFocusable(false);
        doneButton.setBorder(null);
        doneButton.setFont(new Font("Arial", Font.PLAIN, 20));
        doneButton.setBackground(new Color(0XF0F7FD));

        back.setBounds(10, 10, 150, 35);
        back.setFocusable(false);
        back.setBorder(null);
        back.setFont(new Font("Arial", Font.PLAIN, 20));
        back.setBackground(new Color(0X9AB3EC));
        back.setForeground(Color.white);

        firstBackground.add(title);
        firstBackground.add(serialNumber);
        firstBackground.add(serialNumberLabel);
        firstBackground.add(back);

        setSize(1280, 750);
        add(doneButton);
        add(firstBackground);


        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginPage.setCard("CustomerPanel");
            }
        });


        doneButton.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (doneButton.isSelected()) {
                    if (!serialNumber.getText().isBlank()) {
                        try {
                            Ticket t = new Ticket(LoginPage.getPassenger(), serialNumber.getText());
                            info[0].setText(LoginPage.getPassenger().getFirstName());
                            info[1].setText(LoginPage.getPassenger().getLastName());
                            info[2].setText(String.valueOf(t.getFlight().getFlightNum()));
                            info[3].setText(t.getFlight().getOrigin());
                            info[4].setText(t.getFlight().getDestination());
                            info[5].setText(t.getFlight().getFlightDate());
                            info[6].setText(t.getFlight().getFlightTime());
                            info[7].setText(t.getFlight().getPrice());
                            info[8].setText(t.getFlight().getFlightSerialNumber());
                            info[9].setText(t.getSerialNumber());
                            if (doneButton.isSelected()) {
                                doneButton.setText("Back");
                                remove(firstBackground);
                                add(secondBackground);
                            }

                        } catch (Exception a) {
                            JOptionPane.showMessageDialog(new JPanel(), "Could not find the ticket Something went wrong! \n Please try again", "Error!", JOptionPane.ERROR_MESSAGE);
                            doneButton.setSelected(false);
                        }
                    } else {
                        JOptionPane.showMessageDialog(new JPanel(), "the field cannot be empty!!", "Error!", JOptionPane.ERROR_MESSAGE);
                        doneButton.setSelected(false);
                    }
                } else {
                    doneButton.setText("Done!");
                    remove(secondBackground);
                    add(firstBackground);
                }
                revalidate();
                repaint();
            }
        });
    }

}

