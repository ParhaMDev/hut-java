package GUI;

import function.Passenger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

import static function.Person.exists;

public class LoginForm extends JLabel implements ActionListener {

    private JTextField usernameField = new JTextField();
    private JPasswordField passwordField = new JPasswordField();
    private JButton loginButton = new JButton("Login");
    private JButton signUpButton = new JButton("sign up");


    LoginForm() {

        ImageIcon image = new ImageIcon("src/GUI/images/loginpage.jpg");


        JLabel loginText = new JLabel("Login to your Account");
        loginText.setFont(new Font("Oswald", Font.PLAIN, 30));
        loginText.setForeground(new Color(0x1F63AA));
        loginText.setBounds(945, 70, 1400, 100);


        usernameField.setBounds(1020, 320, 200, 25);
        passwordField.setBounds(1020, 400, 200, 25);


        JLabel userIDLabel = new JLabel("Username");
        JLabel passwordLabel = new JLabel("Password");


        userIDLabel.setBounds(930, 318, 75, 25);
        passwordLabel.setBounds(930, 399, 75, 25);


        userIDLabel.setForeground(new Color(0X5F95D3));
        passwordLabel.setForeground(new Color(0X5F95D3));


        userIDLabel.setFont(new Font("sans", Font.PLAIN, 16));
        passwordLabel.setFont(new Font("sans", Font.PLAIN, 16));


        loginButton.setForeground(Color.white);
        loginButton.setFont(new Font("Arial", Font.PLAIN, 18));
        loginButton.setBackground(new Color(0x1F63AA));
        loginButton.setBounds(1100, 500, 95, 40);
        loginButton.setFocusable(false);
        loginButton.addActionListener(this);

        signUpButton.setForeground(Color.white);
        signUpButton.setFont(new Font("Arial", Font.PLAIN, 18));
        signUpButton.setBackground(new Color(0x1F63AA));
        signUpButton.setBounds(980, 500, 95, 40);
        signUpButton.setFocusable(false);
        signUpButton.addActionListener(this);

        setOpaque(true);
        setBackground(new Color(0XE8F2FB));
        setIcon(image);
        setLayout(null);
        setSize(1280, 728);
        add(loginText);
        add(userIDLabel);
        add(passwordLabel);
        add(loginButton);
        add(signUpButton);
        add(usernameField);
        add(passwordField);
        add(loginText);

    }

    boolean isEmpty() {

        String message = "";

        if (String.valueOf(this.passwordField.getPassword()).isBlank()) {
            message += "password field\n";
        }

        if (this.usernameField.getText().isBlank()) {
            message += "username field";
        }

        if (!message.isBlank()) {
            JOptionPane.showMessageDialog(new JPanel(), message
                    , "CANNOT BE EMPTY!!", JOptionPane.ERROR_MESSAGE);
            return true;
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Objects.equals(loginButton, e.getSource())) {

            if (!isEmpty()) {

                switch (exists(String.valueOf(passwordField.getPassword())
                        , usernameField.getText(), true)) {

                    case 1: {
                        try {
                            LoginPage.setPassenger(new Passenger(usernameField.getText()));
                            LoginPage.setCard("CustomerPanel");
                        } catch (Exception a) {
                            JOptionPane.showMessageDialog(new JPanel()
                                    , "There was an error while logging in!\n Please try later!"
                                    , "Error!", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    }

                    case 2: {
                        LoginPage.setCard("adminPanel");
                        break;
                    }

                    case 0: {
                        JOptionPane.showMessageDialog(new JPanel(), "Wrong password or Username"
                                , "USER NOT FOUND!", JOptionPane.ERROR_MESSAGE);
                        break;
                    }

                    case 5: {
                        JOptionPane.showMessageDialog(new JPanel()
                                , "There was an error while logging in!\n Please try later!"
                                , "Error!", JOptionPane.ERROR_MESSAGE);
                        break;
                    }
                }
            }
        }
        if (Objects.equals(signUpButton, e.getSource())) {
            LoginPage.setCard("signUp");
        }
    }
}
