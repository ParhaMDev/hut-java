package GUI;

import function.All;
import function.FlightAttendant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Objects;

public class FAManagement extends JLabel implements ActionListener {
    All allFA;
    All.Types orderBy= All.Types.FLIGHT_ATTENDANTS_ORDER_BY_PERSONAL_CODE;
    boolean asc=true;

    ArrayList<EProfile> profiles = new ArrayList<>();

    JButton back = new JButton("Back");

    JButton newEmployee = new JButton("+");

    final String[] data = new String[]{"first name", "last name", "national id", "birthdate", "date of employment"};
    JComboBox order = new JComboBox(data);

    JRadioButton ascB = new JRadioButton("ascending");
    JRadioButton descB = new JRadioButton("descending");
    JTextField firstName = new JTextField();
    JTextField lastName = new JTextField();
    JTextField birthdate = new JTextField();
    JTextField nationalId = new JTextField();
    JTextField date = new JTextField();

    public FAManagement() {

        newEmployee.setFont(new Font("Sans", Font.BOLD, 50));
        newEmployee.setBounds(1150, 0, 60, 60);
        newEmployee.setBorder(null);
        newEmployee.setBackground(new Color(0x2B8BBF));
        newEmployee.setForeground(Color.white);
        newEmployee.setFocusable(false);
        newEmployee.addActionListener(this);

        ascB.setBounds(450, 10, 100, 30);
        ascB.setBackground(new Color(0x2B8BBF));
        ascB.setForeground(Color.white);
        ascB.setSelected(true);
        ascB.setFocusable(false);
        ascB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ascB.isSelected()) {
                    descB.setSelected(false);
                    asc = true;
                    update();
                }
            }
        });
        descB.setBounds(650, 10, 100, 30);
        descB.setBackground(new Color(0x2B8BBF));
        descB.setForeground(Color.white);
        descB.setFocusable(false);
        descB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(descB.isSelected()){
                    ascB.setSelected(false);
                    asc=false;
                    update();
                }
            }
        });

        back.setBounds(50, 10, 100, 30);
        back.setFocusable(false);
        back.setFont(new Font("Oswald", Font.PLAIN, 20));
        back.setBorder(BorderFactory.createEmptyBorder());
        back.setBackground(Color.white);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginPage.setCard("adminPanel");
            }
        });


        order.setBounds(200, 10, 200, 30);
        ((JLabel) order.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
        order.setFont(new Font("Arial", Font.PLAIN, 20));
        order.setBackground(new Color(0XEFFBFF));
        order.addActionListener(this);

        update();

        setOpaque(true);
        setBackground(new Color(0x2B8BBF));
    }

    void update() {
        allFA=new All(orderBy,asc);
        profiles.clear();

        this.removeAll();
        for (int i = 0, y = 0, x = 110; i < allFA.flightAttendants.size(); i++) {

            profiles.add(new EProfile(allFA.flightAttendants.get(i)));

            profiles.get(i).setBounds(x, 120 + y, 280, 410);
            if (profiles.get(i).getX() < 890) {
                x += 390;
            } else {
                x = 110;
                y += 460;
            }
            this.add(profiles.get(i));
            int finalI = i;
            profiles.get(i).delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    int reply = JOptionPane.showConfirmDialog(null
                            , "Are you sure you want to fire this employee?" +
                                    "\n We will not be able th bring it back", "Cancel?"
                            , JOptionPane.YES_NO_OPTION);

                    if (reply == JOptionPane.YES_OPTION) {
                        try {
                            profiles.get(finalI).flightAttendant.delete();
                            update();
                        } catch (Exception a) {
                            JOptionPane.showMessageDialog(new JPanel()
                                    , "Could not fire the person!" +
                                            " \n Please try again", "Error!", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            });
        }
        setPreferredSize(new Dimension(1280
                , (profiles.size() == 0) ? 0 : profiles.get(profiles.size() - 1).getY() + 410));

        add(ascB);
        add(descB);
        add(order);
        add(back);
        add(newEmployee);
        revalidate();
        repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Objects.equals(newEmployee, e.getSource())) {

            Object[] message = {"first name", firstName
                    , "last name", lastName
                    , "birthdate", birthdate
                    , "national id", nationalId
                    , "date of employment", date};

            int option = JOptionPane.showConfirmDialog(null
                    , message, "new employee", JOptionPane.OK_CANCEL_OPTION);
            if (option == JOptionPane.OK_OPTION) {
                if (!firstName.getText().isBlank()
                        && !lastName.getText().isBlank()
                        && !birthdate.getText().isBlank()
                        && !date.getText().isBlank()
                        && !nationalId.getText().isBlank()) {
                    try {

                        new FlightAttendant(firstName.getText()
                                , lastName.getText(), birthdate.getText()
                                , nationalId.getText(), date.getText());

                        update();
                    } catch (Exception exception) {
                        JOptionPane.showMessageDialog(null
                                , exception + "there was an error! please try again"
                                , "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null
                            , "fields cannot be empty!"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        if (Objects.equals(order, e.getSource())) {
            switch (order.getSelectedIndex()) {
                case 0: {
                    orderBy = All.Types.FLIGHT_ATTENDANTS_ORDER_BY_FIRST_NAME;
                    update();
                    break;
                }
                case 1: {
                    orderBy = All.Types.FLIGHT_ATTENDANTS_ORDER_BY_LAST_NAME;
                    update();
                    break;
                }
                case 2: {
                    orderBy = All.Types.FLIGHT_ATTENDANTS_ORDER_BY_NATIONAL_ID;
                    update();
                    break;
                }
                case 3: {
                    orderBy =All.Types.FLIGHT_ATTENDANTS_ORDER_BY_BIRTHDATE;
                    update();
                    break;
                }
                case 4: {
                    orderBy =All.Types.FLIGHT_ATTENDANTS_ORDER_BY_DATE_OF_EMPLOYMENT;
                    update();
                    break;
                }
            }
        }
    }
}
