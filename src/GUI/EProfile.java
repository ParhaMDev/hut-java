package GUI;


import function.FlightAttendant;
import function.Pilot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class EProfile extends JLabel {
    JButton delete = new JButton("Delete"), edit = new JButton("Edit");
    FlightAttendant flightAttendant;
    Pilot pilot;
    JLabel firstName=new JLabel(), lastName= new JLabel()
            , birthdate=new JLabel(), nationalId=new JLabel()
            , personalCode=new JLabel(), dateOfEmployment=new JLabel();

    public EProfile(FlightAttendant a) {

        flightAttendant = a;
        setSize(280, 410);
        setBackground(new Color(0x9BD2F8));
        setBorder(BorderFactory.createEtchedBorder());
        setOpaque(true);

        delete.setBounds(20, 20, 60, 20);
        delete.setFont(new Font("Sans", Font.PLAIN, 20));
        delete.setBackground(new Color(0x9BD2F8));
        delete.setBorder(BorderFactory.createEmptyBorder());
        delete.setFocusable(false);

        edit.setBounds(220, 20, 40, 20);
        edit.setFont(new Font("Sans", Font.PLAIN, 20));
        edit.setBackground(new Color(0x9BD2F8));
        edit.setBorder(BorderFactory.createEmptyBorder());
        edit.setFocusable(false);

        firstName.setText(" first name : " + flightAttendant.getFirstName());
        firstName.setBounds(20, 60, 340, 30);
        firstName.setFont(new Font("Sans", Font.PLAIN, 15));

        lastName.setText(" last name : " + flightAttendant.getLastName());
        lastName.setBounds(20, 110, 340, 30);
        lastName.setFont(new Font("Sans", Font.PLAIN, 15));

        birthdate.setText(" birthdate : " + flightAttendant.getBirthdate());
        birthdate.setBounds(20, 160, 340, 30);
        birthdate.setFont(new Font("Sans", Font.PLAIN, 15));

        nationalId.setText(" national id : " + flightAttendant.getNationalId());
        nationalId.setBounds(20, 210, 340, 30);
        nationalId.setFont(new Font("Sans", Font.PLAIN, 15));

        personalCode.setText(" personal code : " + flightAttendant.getPersonalCode());
        personalCode.setBounds(20, 260, 340, 30);
        personalCode.setFont(new Font("Sans", Font.PLAIN, 15));

        dateOfEmployment.setText(" date of employment : " + flightAttendant.getDateOfEmployment());
        dateOfEmployment.setBounds(20, 310, 340, 30);
        dateOfEmployment.setFont(new Font("Sans", Font.PLAIN, 15));

        JLabel flightNum=new JLabel("flight number : "+flightAttendant.getFlightNum());
        flightNum.setBounds(20, 360, 340, 30);
        flightNum.setFont(new Font("Sans", Font.PLAIN, 15));

        add(firstName);
        add(lastName);
        add(birthdate);
        add(nationalId);
        add(personalCode);
        add(dateOfEmployment);
        add(flightNum);
        add(delete);
        add(edit);

        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField newFirstName = new JTextField(flightAttendant.getFirstName());
                JTextField newLastName = new JTextField(flightAttendant.getLastName());
                JTextField newBirthdate = new JTextField(flightAttendant.getBirthdate());
                JTextField newNationalId = new JTextField(flightAttendant.getNationalId());
                JTextField newDate = new JTextField(flightAttendant.getDateOfEmployment());
                JTextField newFlight = new JTextField(flightAttendant.getFlightNum());
                Object[] message = {"first name", newFirstName
                        , "last name", newLastName
                        , "birthdate", newBirthdate
                        , "national id", newNationalId
                        , "date of employment", newDate
                        , "flight", newFlight};

                int option = JOptionPane.showConfirmDialog(null, message
                        , "Edit info", JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION) {
                    if (!newLastName.getText().isBlank()
                            && !newBirthdate.getText().isBlank()
                            && !newNationalId.getText().isBlank()
                            && !newDate.getText().isBlank()
                            && !newFirstName.getText().isBlank()) {
                        try {
                            flightAttendant.edit(FlightAttendant.Change.FIRST_NAME, newFirstName.getText());
                            flightAttendant.edit(FlightAttendant.Change.LAST_NAME, newLastName.getText());
                            flightAttendant.edit(FlightAttendant.Change.BIRTHDATE, newBirthdate.getText());
                            flightAttendant.edit(FlightAttendant.Change.NATIONAL_ID, newNationalId.getText());
                            flightAttendant.edit(FlightAttendant.Change.DATE_OF_EMPLOYMENT, newDate.getText());
                            flightAttendant.edit(FlightAttendant.Change.FLIGHT_NUMBER, newFlight.getText());

                            flightNum.setText(" flight number : " + flightAttendant.getFlightNum());
                            lastName.setText("last name : "+flightAttendant.getLastName());
                            birthdate.setText("birthdate : "+flightAttendant.getBirthdate());
                            nationalId.setText("national id : "+flightAttendant.getNationalId());
                            dateOfEmployment.setText("date of employment : "+flightAttendant.getDateOfEmployment());
                            firstName.setText("first name :  "+flightAttendant.getFirstName());

                        } catch (Exception exception) {
                            JOptionPane.showMessageDialog(null
                                    , "there was an error! please try again"
                                    , "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null
                                , "fields cannot be empty!"
                                , "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

    }

    public EProfile(Pilot a) {
        pilot = a;
        setSize(280, 300);
        setBackground(new Color(0x9BD2F8));
        setBorder(BorderFactory.createEtchedBorder());
        setOpaque(true);

        delete.setBounds(20, 20, 60, 20);
        delete.setFont(new Font("Sans", Font.PLAIN, 20));
        delete.setBackground(new Color(0x9BD2F8));
        delete.setBorder(BorderFactory.createEmptyBorder());
        delete.setFocusable(false);

        edit.setBounds(220, 20, 40, 20);
        edit.setFont(new Font("Sans", Font.PLAIN, 20));
        edit.setBackground(new Color(0x9BD2F8));
        edit.setBorder(BorderFactory.createEmptyBorder());
        edit.setFocusable(false);

        firstName.setText(" first name : " + pilot.getFirstName());
        firstName.setBounds(20, 60, 340, 30);
        firstName.setFont(new Font("Sans", Font.PLAIN, 15));

        lastName.setText(" last name : " + pilot.getLastName());
        lastName.setBounds(20, 110, 340, 30);
        lastName.setFont(new Font("Sans", Font.PLAIN, 15));

        birthdate.setText(" birthdate : " + pilot.getBirthdate());
        birthdate.setBounds(20, 160, 340, 30);
        birthdate.setFont(new Font("Sans", Font.PLAIN, 15));

        nationalId.setText(" national id : " + pilot.getNationalId());
        nationalId.setBounds(20, 210, 340, 30);
        nationalId.setFont(new Font("Sans", Font.PLAIN, 15));

        personalCode.setText(" personal code : " +pilot.getPersonalCode());
        personalCode.setBounds(20, 260, 340, 30);
        personalCode.setFont(new Font("Sans", Font.PLAIN, 15));

        dateOfEmployment.setText(" date of employment : " + pilot.getDateOfEmployment());
        dateOfEmployment.setBounds(20, 310, 340, 30);
        dateOfEmployment.setFont(new Font("Sans", Font.PLAIN, 15));

        add(firstName);
        add(lastName);
        add(birthdate);
        add(nationalId);
        add(personalCode);
        add(dateOfEmployment);
        add(delete);
        add(edit);

        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField newFirstName = new JTextField(pilot.getFirstName());
                JTextField newLastName = new JTextField(pilot.getLastName());
                JTextField newBirthdate = new JTextField(pilot.getBirthdate());
                JTextField newNationalId = new JTextField(pilot.getNationalId());
                JTextField newDate = new JTextField(pilot.getDateOfEmployment());
                Object[] message = {"first name", newFirstName
                        , "last name", newLastName
                        , "birthdate", newBirthdate
                        , "national id", newNationalId
                        , "date of employment", newDate};

                int option = JOptionPane.showConfirmDialog(null, message
                        , "Edit info", JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION) {
                    if (!newLastName.getText().isBlank()
                            && !newBirthdate.getText().isBlank()
                            && !newNationalId.getText().isBlank()
                            && !newDate.getText().isBlank()
                            && !newFirstName.getText().isBlank()) {
                        try {
                            pilot.edit(Pilot.Change.FIRST_NAME, newFirstName.getText());
                            pilot.edit(Pilot.Change.LAST_NAME, newLastName.getText());
                            pilot.edit(Pilot.Change.BIRTHDATE, newBirthdate.getText());
                            pilot.edit(Pilot.Change.NATIONAL_ID, newNationalId.getText());
                            pilot.edit(Pilot.Change.DATE_OF_EMPLOYMENT, newDate.getText());

                            lastName.setText("last name : "+pilot.getLastName());
                            birthdate.setText("birthdate : "+pilot.getBirthdate());
                            nationalId.setText("national id : "+pilot.getNationalId());
                            dateOfEmployment.setText("date of employment : "+pilot.getDateOfEmployment());
                            firstName.setText("first name :  "+pilot.getFirstName());

                        } catch (Exception exception) {
                            JOptionPane.showMessageDialog(null
                                    , "there was an error! please try again"
                                    , "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null
                                , "fields cannot be empty!"
                                , "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JOptionPane.showMessageDialog(null, pilot.getFlightsList(),"List of flights",JOptionPane.INFORMATION_MESSAGE);
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

}
