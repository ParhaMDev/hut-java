package GUI;

import function.Plane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class PlaneLine extends JLabel{
    JButton delete = new JButton("Delete"), edit = new JButton("Edit");
    private JLabel productionDate = new JLabel(), capacity = new JLabel(), planeSerialNumber = new JLabel();
    private Plane plane;

    PlaneLine(Plane p) {
        setSize(1180, 50);
        setBackground(new Color(0x9BD2F8));
        setBorder(BorderFactory.createEtchedBorder());
        setOpaque(true);

        plane = p;

        delete.setBounds(1100, 15, 60, 20);
        delete.setFont(new Font("Sans", Font.PLAIN, 20));
        delete.setBackground(new Color(0x9BD2F8));
        delete.setBorder(BorderFactory.createEmptyBorder());
        delete.setFocusable(false);

        edit.setBounds(1030, 15, 40, 20);
        edit.setFont(new Font("Sans", Font.PLAIN, 20));
        edit.setBackground(new Color(0x9BD2F8));
        edit.setBorder(BorderFactory.createEmptyBorder());
        edit.setFocusable(false);

        capacity.setText(" capacity : " + plane.getCapacity());
        capacity.setBounds(30, 15, 200, 20);
        capacity.setFont(new Font("Sans", Font.PLAIN, 20));

        planeSerialNumber.setText(" serial number : " + plane.getPlaneSerialNumber());
        planeSerialNumber.setBounds(300, 15, 350, 20);
        planeSerialNumber.setFont(new Font("Sans", Font.PLAIN, 20));

        productionDate.setText(" production date : " + plane.getProductionDate());
        productionDate.setBounds(700, 15, 210, 20);
        productionDate.setFont(new Font("Sans", Font.PLAIN, 20));

        add(delete);
        add(planeSerialNumber);
        add(capacity);
        add(productionDate);
        add(edit);


        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                 JOptionPane.showMessageDialog(null,plane.getFlightsList());
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

    }


    public Plane getPlane() {
        return plane;
    }

}
