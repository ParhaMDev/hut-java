package GUI;

import function.All;
import function.Plane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class PlaneManagement extends JLabel {

    All allPlanes = new All(All.Types.PLANES,true);

    ArrayList<PlaneLine> lines = new ArrayList<>();

    JButton back = new JButton("Back");

    JButton newPlane = new JButton("+");

    PlaneManagement() {
        JTextField capacity = new JTextField();
        JTextField productionDate = new JTextField();
     
        Object[] message = {
                "capacity", capacity
                , "productionDate", productionDate};

        newPlane.setFont(new Font("Sans", Font.BOLD, 50));
        newPlane.setBounds(1150, 0, 60, 60);
        newPlane.setBorder(null);
        newPlane.setBackground(new Color(0x2B8BBF));
        newPlane.setForeground(Color.white);
        newPlane.setFocusable(false);
        newPlane.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(null
                        , message, "new Plane", JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION) {
                    if (!capacity.getText().isBlank()
                            && !productionDate.getText().isBlank()) {
                        try {
                            new Plane(capacity.getText() , productionDate.getText());
                            update();
                        } catch (Exception exception) {
                            JOptionPane.showMessageDialog(null
                                    , "there was an error! please try again"
                                    , "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null
                                , "fields cannot be empty!"
                                , "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        back.setBounds(50, 10, 100, 30);
        back.setFocusable(false);
        back.setFont(new Font("Oswald", Font.PLAIN, 20));
        back.setBorder(BorderFactory.createEmptyBorder());
        back.setBackground(Color.white);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginPage.setCard("adminPanel");
            }
        });

        update();

        setOpaque(true);
        setBackground(new Color(0x2B8BBF));
    }

    void update() {
        allPlanes = new All(All.Types.PLANES,true);

        lines.clear();

        this.removeAll();
        for (int i = 0, y = 0; i < allPlanes.planes.size(); i++, y += 100) {
            lines.add(new PlaneLine(allPlanes.planes.get(i)));
            lines.get(i).setBounds(50, 120 + y, 1180, 50);
            this.add(lines.get(i));
            int finalI = i;
            lines.get(i).delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    int reply = JOptionPane.showConfirmDialog(null
                            , "Are you sure you want to delete this plane?" +
                                    "\n We will not be able th bring it back", "Cancel?"
                            , JOptionPane.YES_NO_OPTION);

                    if (reply == JOptionPane.YES_OPTION) {
                        try {
                            lines.get(finalI).getPlane().delete();
                            update();
                        } catch (Exception a) {
                            JOptionPane.showMessageDialog(new JPanel()
                                    , "Could not delete the plane Something went wrong!" +
                                            " \n Please try again", "Error!", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            });
        }
        setPreferredSize(new Dimension(1280
                , (lines.size() == 0) ? 0 : lines.get(lines.size() - 1).getY() + 100));

        add(back);
        add(newPlane);
        revalidate();
        repaint();
    }
}
