package function;

import java.sql.*;
import java.util.ArrayList;

public class  Pilot extends Person {

    public enum Change {
        FIRST_NAME,
        LAST_NAME,
        NATIONAL_ID,
        BIRTHDATE,
        DATE_OF_EMPLOYMENT;
        static private String getChange(Change c){//returns the name of the column which should be changed
            switch (c) {
                case FIRST_NAME: return "first_name";
                case LAST_NAME: return "last_name";
                case NATIONAL_ID: return "national_id";
                case BIRTHDATE: return "birthdate";
                case DATE_OF_EMPLOYMENT: return "date_of_employment";
            }
            return "";
        }
    }

    private String dateOfEmployment;
    private String personalCode;
    private ArrayList<String> flightsList = new ArrayList<>();

    public Pilot(String nationalId) throws Exception {//creating a  new pilot object using saved data
        Connection connect = null;//to connect to the database
        Statement statement = null;//the execute the data
        ResultSet result = null;//to get and use the tabular data

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting to the database
        statement = connect.createStatement();

        //importing data from the database
        result = statement.executeQuery("SELECT id,first_name"//selecting data
                + ",last_name"
                + ",birthdate"
                + ",national_id"
                + ",date_of_employment"
                + " FROM pilots_info"
                + " WHERE national_id ='" + nationalId + "'");

        this.firstName = result.getString("first_name");
        this.lastName = result.getString("last_name");
        this.birthdate = result.getString("birthdate");
        this.nationalId = result.getString("national_id");
        this.personalCode = result.getString("id");
        this.dateOfEmployment = result.getString("date_of_employment");

        result = statement.executeQuery("SELECT flight_serial_number"//selecting the flights list to use in the ArrayList
                + " FROM flights_info"
                + " INNER JOIN pilots_info on(flights_info.pilot_id = pilots_info.id"
                + " AND pilots_info.id= '" + this.personalCode + "')");

        while (result.next()) {//creating a list of flights
            this.flightsList.add(result.getString("flight_serial_number"));
        }

        connect.close();
        statement.close();
        result.close();

    }

    public Pilot(String firstName,
                 String lastName,
                 String birthdate,
                 String nationId,
                 String dateOfEmployment) throws Exception {//creating new pilot object which did not exist before
        super(firstName, lastName, birthdate, nationId);//using the parent class constructor
        this.dateOfEmployment = dateOfEmployment;
        save();
    }

    private void save() throws Exception {//to save new pilot in the database which does not exist
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting
        statement = connect.createStatement();

        statement.executeUpdate("INSERT INTO pilots_info"//inserting data
                + " (first_name"
                + ",last_name"
                + ",birthdate"
                + ",national_id"
                + ",date_of_employment )"
                + " SELECT '"
                + this.firstName + "','"
                + this.lastName + "','"
                + this.birthdate + "','"
                + this.nationalId + "','"
                + this.dateOfEmployment + "'");

        connect.close();
        statement.close();

    }
    public void edit(Change _change, String with) throws Exception {//edit the pilot by the parameters
        String change=Change.getChange(_change);

        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        statement.executeUpdate("UPDATE pilots_info"//updating data
                + " SET '" + change + "' = '" + with
                + "' WHERE national_id = '" + this.nationalId + "'");

        if (change == "national_id") {
            //we will upload the object form database again so we need new national_id to find the data
            this.nationalId = with;
        }

        Pilot p=new Pilot(this.nationalId);//uploading edited data from database
        this.firstName=p.firstName;
        this.lastName=p.lastName;
        this.birthdate=p.birthdate;
        this.dateOfEmployment=p.dateOfEmployment;

        connect.close();
        statement.close();
    }


    public void delete() throws Exception {//to delete the object from the database
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEsT.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");
        //dropping the row that its serial number matches the serial number in the object
        statement.executeUpdate("DELETE FROM pilots_info WHERE national_id= '" + nationalId + "'");//dropping the row which its nation id matches the nation id in the object

        connect.close();
        statement.close();

    }

    public String getPersonalCode() {
        return personalCode;
    }

    public String getDateOfEmployment() {
        return dateOfEmployment;
    }

    public String getFlightsList() {
        String message = "";
        for(int i=0;i<flightsList.size();i++){
            message+=flightsList.get(i)+"\n";
        }
        return message;
    }
}
