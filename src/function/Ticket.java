package function;

import GUI.LoginPage;

import java.sql.*;
import java.util.Random;

public class Ticket {
    private Flight flight;
    private Passenger passenger;
    private String serialNumber;
    private int price;

    public Ticket(Passenger passenger, String serialNumber) throws Exception {//creating a ticket object using database
        Connection connect = null;//to connect to the database
        Statement statement = null;//to execute queries
        ResultSet result = null;//to get the data and convert the tabular data form the database to Strings or int etc

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        //importing data from the database
        result = statement.executeQuery("SELECT tickets.flight_num ,price " +
                "FROM tickets INNER JOIN passengers_info ON" +
                " ( tickets.passenger=passengers_info.id " +
                "AND passengers_info.national_id ='" + passenger.nationalId + "'" +
                "AND tickets.serial_number=" + serialNumber + ")");

        this.price = result.getInt("price");
        this.serialNumber = serialNumber;
        this.passenger = passenger;

        result = statement.executeQuery("SELECT flight_serial_number" +
                " FROM flights_info" +
                " WHERE flight_num = " + result.getInt("flight_num"));
        this.flight = new Flight(result.getString("flight_serial_number"));

        connect.close();
        statement.close();
        result.close();

    }

    public Ticket(Passenger passenger, Flight flight) throws Exception {//creating new ticket that did not exist
        this.flight = flight;
        this.passenger = passenger;
        this.price = Integer.parseInt(this.flight.getPrice());
        save();
    }

    private void save() throws Exception {//to save new ticket in the database which does not exist
        Connection connect;
        Statement statement;
        ResultSet result = null;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting
        statement = connect.createStatement();

        long serial = 0;//a long variable for random serial
        int check = 1;//an integer to get the number of matched serial numbers , if it's 0 it means the serial number is unique
        Random rand = new Random();

        while (check != 0) {
            serial = rand.nextInt(1000000000);//random number
            result = statement.executeQuery("SELECT COUNT(*)" +
                    " FROM tickets WHERE serial_number=" + serial);//checking if the serial number exists or not
            check = result.getInt(1);//getting the number of matched serial numbers
        }

        this.serialNumber = String.valueOf(serial);

        statement.executeUpdate("INSERT INTO tickets"//inserting data
                + " (serial_number ,flight_num" +
                " , passenger "
                + ",price)"
                + " SELECT "
                + this.serialNumber + " , "
                + this.flight.getFlightNum() + " , "
                + this.passenger.getId() + " , "
                + this.price);

        this.flight.edit(Flight.Change.INCOME, (this.flight.getIncome() + this.price) + "");
        this.flight = new Flight(this.flight.getFlightSerialNumber());

        connect.close();
        statement.close();
        result.close();
    }

    public void delete() throws Exception {//to delete and cancel a ticket
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        //dropping the row that its passenger matches the passenger in the object
        statement.executeUpdate("DELETE FROM tickets" +
                " WHERE passenger='" + this.passenger.getId() + "'");
        connect.close();
        statement.close();
    }

    public String getTicketInfo() {
        return "passenger's name :" + LoginPage.getPassenger().firstName + " " + LoginPage.getPassenger().lastName + "\n flight date : "
                + flight.getFlightDate() + "\n flight time : " + flight.getFlightTime() + "\n from : " + flight.getOrigin() + "\n to : "
                + flight.getDestination() + "\n serial number : " + this.serialNumber
                + "\n flight serial number : " + this.flight.getFlightSerialNumber() + "\n flight number : " + this.flight.getFlightNum()
                + "\n the pilot's personal code : " + this.flight.getThePilot();
    }
    public Flight getFlight(){
        return flight;
    }
    public String getSerialNumber() {
        return serialNumber;
    }
}
