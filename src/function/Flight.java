package function;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

public class Flight {//defining variables

    public enum Change {
        PLANE,
        ORIGIN,
        DESTINATION,
        FLIGHT_DATE,
        FLIGHT_TIME,
        INCOME,
        PILOT,
        PRICE;
        static private String getChange(Change c){//returns the name of the column which should be changed
            switch (c) {
                case PLANE: return "plane_id";
                case ORIGIN: return "origin";
                case DESTINATION: return "destination";
                case FLIGHT_DATE: return "date";
                case FLIGHT_TIME: return "flight_time";
                case INCOME: return "income";
                case PILOT: return "pilot_id";
                case PRICE: return "price";
            }
            return "";
        }
    }

    private String flightSerialNumber;
    private String planeId;
    private String from;
    private String to;
    private String flightDate;
    private String flightTime;
    private int numberOfPassengers;
    private ArrayList<Passenger> passengers = new ArrayList<>();
    private ArrayList<FlightAttendant> flightAttendants = new ArrayList<>();
    private Pilot thePilot;
    private int income;
    private int flightNum;
    private String price;

    public Flight(String flightSerialNumber) throws Exception {//creating a new flight object using saved data
        Connection connect = null;//to connect to the database
        Statement statement = null;//to execute queries
        ResultSet result = null;//to get the data and convert the tabular data form the database to Strings or int etc

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting to the database
        statement = connect.createStatement();//creating the statement

        //importing data from the database
        result = statement.executeQuery("SELECT flight_serial_number " +
                ",plane_id" +
                ",origin" +
                ",destination" +
                ",date" +
                ",flight_time" +
                ",income" +
                ",flight_num" +
                ",price" +
                " From flights_info" +
                " WHERE flights_info.flight_serial_number= '" + flightSerialNumber + "'");

        this.flightSerialNumber = result.getString("flight_serial_number");
        this.planeId = result.getString("plane_Id");
        this.from = result.getString("origin");
        this.to = result.getString("destination");
        this.flightDate = result.getString("date");
        this.flightTime = result.getString("flight_time");
        this.income = result.getInt("income");
        this.flightNum = result.getInt("flight_num");
        this.price = result.getString("price");

        //importing the pilot's info to create the Pilot object
        result = statement.executeQuery("SELECT national_id FROM pilots_info" +
                " INNER JOIN flights_info ON ( pilots_info.id = flights_info.pilot_id" +
                " AND flights_info.flight_serial_number = '" + flightSerialNumber + "')");

        this.thePilot = new Pilot(result.getString("national_id"));

        //importing the passengers info to create a list of them
        result = statement.executeQuery("SELECT national_id FROM passengers_info" +
                " INNER JOIN tickets INNER JOIN flights_info" +
                " On(passengers_info.id = tickets.passenger AND" +
                " tickets.flight_num = flights_info.flight_num AND" +
                " flight_serial_number = '" + flightSerialNumber + "') ");

        while (result.next()) {//creating the list
            this.passengers.add(new Passenger(result.getString("national_id")));
        }

        //importing the flight attendants info to create a list of the
        result = statement.executeQuery("SELECT national_id FROM" +
                " flight_attendants_info INNER JOIN flights_info" +
                " ON(flights_info.flight_num = flight_attendants_info.flight_num" +
                " AND flights_info.flight_serial_number ='" + flightSerialNumber + "')");

        while (result.next()) {//creating the list
            this.flightAttendants.add(new FlightAttendant(result.getString("national_id")));
        }

        this.numberOfPassengers = passengers.size();//using the length of passengers' ArrayList to have the number of them

        connect.close();
        statement.close();
        result.close();

    }

    public Flight(String planeId,
                  String from,
                  String to,
                  String flightDate,
                  String flightTime,
                  String pilotNationId,
                  String price) throws Exception {
        this.planeId = planeId;
        this.from = from;
        this.to = to;
        this.flightDate = flightDate;
        this.flightTime = flightTime;
        this.thePilot = new Pilot(pilotNationId);
        this.price = price;
        save();
    }//creating new flight which did not exist before


    private void save() throws Exception {//used to save new object in database
        Connection connect;//to connect to the database
        Statement statement;//to execute queries
        ResultSet result=null;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        long serial = 0;//a long variable for random serial
        int check = 1;//an integer to get the number of matched serial numbers , if it's 0 it means the serial number is unique
        Random rand=new Random();

        while (check != 0) {
            serial = rand.nextInt(1000000000);//random number
            result = statement.executeQuery("SELECT COUNT(*)" +
                    " FROM flights_info WHERE flight_serial_number=" + serial );//checking if the serial number exists or not
            check = result.getInt(1);//getting the number of matched serial numbers
        }

        this.flightSerialNumber = String.valueOf(serial);

        statement.executeUpdate("INSERT INTO flights_info"
                + " (flight_serial_number"
                + ",plane_id"
                + ",origin"
                + ",destination"
                + ",date"
                + ",flight_time"
                + ",pilot_id"
                + ",price )"
                + " SELECT '"
                + this.flightSerialNumber + "','"
                + this.planeId + "','"
                + this.from + "','"
                + this.to + "','"
                + this.flightDate + "','"
                + this.flightTime + "',"
                + this.thePilot.getPersonalCode() + " , '"
                + this.price + "'");

        connect.close();
        statement.close();
        result.close();

    }

    public void edit(Change _change, String with) throws Exception {//to edit the flight by the parameters
        String change=Change.getChange(_change);

        Connection connect;
        Statement statement;
        ResultSet result;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        statement.executeUpdate("UPDATE flights_info"//updating data
                + " SET '" + change + "' = '" + with
                + "' WHERE flight_serial_number = '" + this.flightSerialNumber + "'");

        Flight f = new Flight(this.flightSerialNumber);//uploading edited data from database
        this.planeId = f.planeId;
        this.from = f.from;
        this.to = f.to;
        this.flightDate = f.flightDate;
        this.flightTime = f.flightTime;
        this.price = f.price;

        result = statement.executeQuery("SELECT national_id FROM pilots_info" +
                " INNER JOIN flights_info ON ( pilots_info.id = flights_info.pilot_id" +
                " AND flights_info.flight_serial_number = '" + this.flightSerialNumber + "')");

        this.thePilot = new Pilot(result.getString("national_id"));

        connect.close();
        statement.close();
        result.close();

    }

    public void delete() throws Exception {//to delete and cancel a flight
        Connection connect;
        Statement statement ;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        //dropping the row that its serial number matches the serial number in the object
        statement.executeUpdate("DELETE FROM flights_info" +
                " WHERE flight_serial_number='" + this.flightSerialNumber + "'");
        connect.close();
        statement.close();

    }

    public String getPassengers() {
        String message = "";
        for (int i = 0; i < passengers.size(); i++) {
            message += passengers.get(i).nationalId + "\n";
        }
        return message;
    }

    public String getFlightAttendants() {
        String message = "";
        for (int i = 0; i < flightAttendants.size(); i++) {
            message += flightAttendants.get(i).getPersonalCode() + "\n";
        }
        return message;
    }

    public String getFlightInfo() {
        return " flight date : "
                + flightDate + "\n flight time : " + flightTime + "\n from : " + from + "\n to : "
                + to + "\n price : " + price;
    }

    public String getThePilot() {
        return thePilot.getPersonalCode();
    }

    public String getAllPilotInfo() {
        return
                "first name : " + thePilot.getFirstName()
                        + "\n lase  name : " + thePilot.getLastName()
                        + "\n personal code : " + thePilot.getPersonalCode()
                        + "\n national Id : " + thePilot.getNationalId()
                        + "\n birthdate : " + thePilot.getBirthdate()
                        + "\n date of employment : " + thePilot.getDateOfEmployment();
    }

    public int getIncome() {
        return income;
    }

    public int getFlightNum() {
        return flightNum;
    }

    public String getPrice() {
        return price;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public String getFlightTime() {
        return flightTime;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public String getOrigin() {
        return from;
    }

    public String getDestination() {
        return to;
    }

    public String getPlaneId() {
        return planeId;
    }

    public String getFlightSerialNumber() {
        return flightSerialNumber;
    }

}
