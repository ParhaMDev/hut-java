package function;


import java.sql.*;

public class Person {//the parent class of pilot,flight attendant and passenger
    protected String firstName;
    protected String lastName;
    protected String birthdate;
    protected String nationalId;

    public Person(String... str) {
        if (str.length == 4) {
            this.firstName = str[0];
            this.lastName = str[1];
            this.birthdate = str[2];
            this.nationalId = str[3];
        }
    }
    public static int exists(String password,String userName,boolean login) {
        Connection connect=null;
        Statement statement=null;
        ResultSet result=null;
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
            statement = connect.createStatement();
            if(login) {
                result = statement.executeQuery("SELECT password,national_id FROM passengers_info" +
                        " WHERE password='" + password + "' AND national_id='" + userName + "'");
                if (result.isBeforeFirst()) {
                    return 1;
                }
                result = statement.executeQuery("SELECT password , user_name " +
                        "FROM manager WHERE password='" + password + "' AND user_name=" + userName);
                if (result.isBeforeFirst()) {

                    return 2;
                }
            }
            else {
                result = statement.executeQuery("SELECT password FROM passengers_info" +
                        " WHERE password='" + password + "'");
                if (result.isBeforeFirst()) {
                    return 3;
                }

                result = statement.executeQuery("SELECT national_id FROM passengers_info" +
                        " WHERE national_id='" + userName + "'");
                if (result.isBeforeFirst()) {
                    return 4;
                }
            }
        }catch(Exception e){
            return 5;
        }
        finally {
            try{
                connect.close();
                statement.close();
                result.close();
            }catch(Exception e){
                return 5;
            }
        }

        return 0;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public String getBirthdate(){
        return birthdate;
    }
    public String getNationalId(){
        return nationalId;
    }

}