package function;

import function.*;

import javax.swing.*;
import java.sql.*;

public class Passenger extends Person {

    public enum Change {
        FIRST_NAME,
        LAST_NAME,
        FATHER_NAME,
        NATIONAL_ID,
        BIRTHDATE,
        PASSWORD;
        static private String getChange(Change c){//returns the name of the column which should be changed
            switch (c) {
                case FIRST_NAME: return "first_name";
                case LAST_NAME: return "last_name";
                case FATHER_NAME: return "father_name";
                case NATIONAL_ID: return "national_id";
                case BIRTHDATE: return "birthdate";
                case PASSWORD: return "password";
            }
            return "";
        }
    }

    private String fatherName;
    private String password;
    private String id;

    public Passenger(String nationalId) throws Exception {//creating a  new passenger object using saved data
        Connection connect = null;//to connect to the database
        Statement statement = null;//the execute the data
        ResultSet result = null;//to get and use the tabular data

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting to the database
        statement = connect.createStatement();

        //importing data from the database
        result = statement.executeQuery("SELECT id,first_name"//selecting data
                + ",last_name"
                + ",father_name"
                + ",birthdate"
                + ",national_id"
                + ",password"
                + " FROM passengers_info"
                + " WHERE national_id ='" + nationalId + "'");

        this.id = result.getString("id");
        this.firstName = result.getString("first_name");
        this.lastName = result.getString("last_name");
        this.fatherName = result.getString("father_name");
        this.birthdate = result.getString("birthdate");
        this.nationalId = result.getString("national_id");
        this.password = result.getString("password");

        connect.close();
        statement.close();
        result.close();


    }

    public Passenger(String firstName,
                     String lastName,
                     String fatherName,
                     String birthdate,
                     String nationalId,
                     String password) throws Exception{//creating new passenger object which did not exist before
        super(firstName, lastName, birthdate, nationalId);//using the parent class constructor
        this.fatherName = fatherName;
        this.password = password;
        save();
    }

    private void save() throws Exception {//to save new passenger in the database which does not exist
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting
        statement = connect.createStatement();

        statement.executeUpdate("INSERT INTO passengers_info"//inserting data
                + " (first_name"
                + ",last_name"
                + ",father_name"
                + ",birthdate"
                + ",national_id"
                + ",password)"
                + " SELECT '"
                + this.firstName + "','"
                + this.lastName + "','"
                + this.fatherName + "','"
                + this.birthdate + "','"
                + this.nationalId + "','"
                + this.password + "'");

        connect.close();
        statement.close();
    }


    public void edit(Change _change, String with) throws Exception {//edit the passenger by the parameters
        String change=Change.getChange(_change);

        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        statement.executeUpdate("UPDATE passengers_info"//updating data
                + " SET '" + change + "' = '" + with
                + "' WHERE national_id = '" + this.nationalId + "'");

        if (change == "national_id") {
            //we will upload the object form database again so we need new national_id to find the data
            this.nationalId = with;
        }

        Passenger p = new Passenger(this.nationalId);//uploading edited data from database
        this.firstName = p.firstName;
        this.lastName = p.lastName;
        this.fatherName = p.fatherName;
        this.birthdate = p.birthdate;
        this.password = p.password;

        connect.close();
        statement.close();
    }

    public void delete() throws Exception {//to delete the object from the database
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEsT.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        //dropping the row that its serial number matches the serial number in the object
        statement.executeUpdate("DELETE FROM passengers_info WHERE national_id= '" + nationalId + "'");//dropping the row which its nation id matches the nation id in the object
        connect.close();
        statement.close();
    }

    public String getFatherName() {
        return fatherName;
    }

    public String getPassword() {
        return password;
    }

    public String getId() {
        return id;
    }
}
