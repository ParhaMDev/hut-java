package function;

import java.util.ArrayList;
import java.sql.*;
import java.util.Random;

public class Plane {
    private String planeSerialNumber;
    private String capacity;
    private String productionDate;
    private ArrayList<String> flightsList = new ArrayList<>();

    public Plane(String planeSerialNumber) throws Exception {//creating a  new plane object using saved data
        Connection connect = null;//to connect to the database
        Statement statement = null;//the execute the data
        ResultSet result = null;//to get and use the tabular data

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting to the database
        statement = connect.createStatement();

        //importing data from the database
        result = statement.executeQuery("SELECT plane_serial_number"//selecting data
                + ",production_date"
                + ",capacity"
                + " FROM planes_info"
                + " WHERE plane_serial_number ='" + planeSerialNumber + "'");

        this.planeSerialNumber = result.getString("plane_serial_number");
        this.productionDate = result.getString("production_date");
        this.capacity = result.getString("capacity");

        result = statement.executeQuery("SELECT flight_num"//selecting the flights list to use in the ArrayList
                + " FROM flights_info"
                + " INNER JOIN planes_info on(flights_info.plane_id ="
                + " planes_info.id AND"
                + " planes_info.plane_seriaL_number = '" + planeSerialNumber + "')");

        while (result.next()) {//creating a list of flight schedule
            this.flightsList.add(result.getString("flight_num"));
        }

        connect.close();
        statement.close();
        result.close();

    }


    public Plane(String capacity,
                 String productionDate) throws Exception {//creating new plane object which did not exist before
        this.capacity = capacity;
        this.productionDate = productionDate;
        save();
    }

    private void save() throws Exception {//to save new plane in the database which does not exist
        Connection connect;
        Statement statement;
        ResultSet result = null;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting
        statement = connect.createStatement();

        long serial = 0;//a long variable for random serial
        int check = 1;//an integer to get the number of matched serial numbers , if it's 0 it means the serial number is unique
        Random rand = new Random();

        while (check != 0) {
            serial = rand.nextInt(1000000000);//random number
            result = statement.executeQuery("SELECT COUNT(*)" +
                    " FROM planes_info WHERE plane_serial_number=" + serial);//checking if the serial number exists or not
            check = result.getInt(1);//getting the number of matched serial numbers
        }

        this.planeSerialNumber = String.valueOf(serial);

        statement.executeUpdate("INSERT INTO planes_info"//inserting data
                + " (plane_serial_number"
                + ",production_date"
                + ",capacity)"
                + " SELECT '"
                + this.planeSerialNumber + "','"
                + this.productionDate + "','"
                + this.capacity + "'");

        connect.close();
        statement.close();
        result.close();

    }


    public void delete() throws Exception {//to delete the object from the database
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEsT.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        //dropping the row that its serial number matches the serial number in the object
        statement.executeUpdate("DELETE FROM planes_info WHERE plane_serial_number= '"
                + planeSerialNumber + "'");//dropping the row which its serial number matches the serial number in the object

        connect.close();
        statement.close();
    }

    public String getCapacity() {
        return capacity;
    }

    public String getFlightsList() {
        String message = "";
        for (int i = 0; i < flightsList.size(); i++) {
            message += flightsList.get(i) + "\n";
        }
        return message;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public String getPlaneSerialNumber() {
        return planeSerialNumber;
    }
}
