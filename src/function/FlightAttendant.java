package function;

import java.sql.*;

public class FlightAttendant extends Person {

    public enum Change {
        FIRST_NAME,
        LAST_NAME,
        NATIONAL_ID,
        BIRTHDATE,
        FLIGHT_NUMBER,
        DATE_OF_EMPLOYMENT;
        static private String getChange(Change c){//returns the name of the column which should be changed
            switch (c) {
                case FIRST_NAME: return "first_name";
                case LAST_NAME: return "last_name";
                case NATIONAL_ID: return "national_id";
                case BIRTHDATE: return "birthdate";
                case DATE_OF_EMPLOYMENT: return "date_of_employment";
                case FLIGHT_NUMBER: return "flight_num";
            }
            return "";
        }
    }

    private String dateOfEmployment;
    private String personalCode;
    private int flightNum;

    public FlightAttendant(String nationalId) throws Exception {//creating new object using the saved data
        Connection connect = null;//to connect to the database
        Statement statement = null;//the execute queries
        ResultSet result = null;//to get and use the tabular

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting
        statement = connect.createStatement();

        //importing data from the database
        result = statement.executeQuery("SELECT first_name"//selecting data from the database
                + ",last_name"
                + ",birthdate"
                + ",national_id"
                + ",date_of_employment"
                + ",id"
                + " FROM flight_attendants_info"
                + " WHERE national_id = '" + nationalId + "'");

        this.firstName = result.getString("first_name");
        this.lastName = result.getString("last_name");
        this.birthdate = result.getString("birthdate");
        this.nationalId = result.getString("national_id");
        this.personalCode = result.getString("id");
        this.dateOfEmployment = result.getString("date_of_employment");

        connect.close();
        statement.close();
        result.close();

    }

    public FlightAttendant(String firstName,
                           String lastName,
                           String birthdate,
                           String nationalId,
                           String dateOfEmployment) throws Exception {//creating new object which did not exist before
        super(firstName, lastName, birthdate, nationalId);//using the parent constructor
        this.dateOfEmployment = dateOfEmployment;
        save();
    }

    private void save() throws Exception {//to save new object in the database which does not exist
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");//connecting to the database
        statement = connect.createStatement();

        statement.executeUpdate("INSERT INTO flight_attendants_info"//inserting data
                + " (first_name"
                + ",last_name"
                + ",birthdate"
                + ",national_id"
                + ",date_of_employment )"
                + " SELECT '"
                + this.firstName + "','"
                + this.lastName + "','"
                + this.birthdate + "','"
                + this.nationalId + "','"
                + this.dateOfEmployment + "'");

        connect.close();
        statement.close();

    }
    public void edit(Change _change, String with) throws Exception {//edit the flight attendant by the parameters
        String change=Change.getChange(_change);

        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        statement.executeUpdate("UPDATE flight_attendants_info"//updating data
                + " SET '" + change + "' = '" + with
                + "' WHERE national_id = '" + this.nationalId + "'");

        if (change == "national_id") {
            //we will upload the object form database again so we need new national_id to find the data
            this.nationalId = with;
        }

        FlightAttendant f=new FlightAttendant(this.nationalId);//uploading edited data from database
        this.firstName=f.firstName;
        this.lastName=f.lastName;
        this.birthdate=f.birthdate;
        this.flightNum=f.flightNum;
        this.dateOfEmployment=f.dateOfEmployment;

        connect.close();
        statement.close();
    }

    public void delete() throws Exception {//to delete the object from database
        Connection connect;
        Statement statement;

        Class.forName("org.sqlite.JDBC");

        connect = DriverManager.getConnection("jdbc:sqlite:TEsT.db");//connecting
        statement = connect.createStatement();

        statement.executeUpdate("PRAGMA FOREIGN_KEYS = ON");//enabling foreign keys
        //dropping the row that its serial number matches the serial number in the object
        statement.executeUpdate("DELETE FROM flight_attendants_info WHERE" +
                " national_id= '" + nationalId + "'");//dropping the row which its nation id matches the nation id in the object

        connect.close();
        statement.close();

    }

    public String getPersonalCode() {
        return personalCode;
    }

    public String getDateOfEmployment() {
        return dateOfEmployment;
    }

    public int getFlightNum() {
        return flightNum;
    }
}
