package function;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class All {
    public enum Types {//the only available options for user to get sorted data
        FLIGHTS,
        PLANES,
        FLIGHT_ATTENDANTS_ORDER_BY_FIRST_NAME,
        FLIGHT_ATTENDANTS_ORDER_BY_LAST_NAME,
        FLIGHT_ATTENDANTS_ORDER_BY_NATIONAL_ID,
        FLIGHT_ATTENDANTS_ORDER_BY_PERSONAL_CODE,
        FLIGHT_ATTENDANTS_ORDER_BY_DATE_OF_EMPLOYMENT,
        FLIGHT_ATTENDANTS_ORDER_BY_BIRTHDATE,
        PILOTS_ORDER_BY_FIRST_NAME,
        PILOTS_ORDER_BY_LAST_NAME,
        PILOTS_ORDER_BY_NATIONAL_ID,
        PILOTS_ORDER_BY_PERSONAL_CODE,
        PILOTS_ORDER_BY_DATE_OF_EMPLOYMENT,
        PILOTS_ORDER_BY_BIRTHDATE;

        static private int getOption(Types t) {//returns the wanted object such as flight
            switch (t) {
                case FLIGHTS:
                    return 1;
                case PLANES:
                    return 4;
                case FLIGHT_ATTENDANTS_ORDER_BY_FIRST_NAME:
                case FLIGHT_ATTENDANTS_ORDER_BY_LAST_NAME:
                case FLIGHT_ATTENDANTS_ORDER_BY_NATIONAL_ID:
                case FLIGHT_ATTENDANTS_ORDER_BY_PERSONAL_CODE:
                case FLIGHT_ATTENDANTS_ORDER_BY_DATE_OF_EMPLOYMENT:
                case FLIGHT_ATTENDANTS_ORDER_BY_BIRTHDATE:
                    return 2;
                case PILOTS_ORDER_BY_FIRST_NAME:
                case PILOTS_ORDER_BY_LAST_NAME:
                case PILOTS_ORDER_BY_NATIONAL_ID:
                case PILOTS_ORDER_BY_PERSONAL_CODE:
                case PILOTS_ORDER_BY_DATE_OF_EMPLOYMENT:
                case PILOTS_ORDER_BY_BIRTHDATE:
                    return 3;
            }
            return 0;
        }

        static private String getOrderBy(Types t) {//returns the column that data should be ordered by
            switch (t) {
                case FLIGHTS: return "flight_num";

                case PLANES:
                case FLIGHT_ATTENDANTS_ORDER_BY_PERSONAL_CODE:
                case PILOTS_ORDER_BY_PERSONAL_CODE: return "id";

                case PILOTS_ORDER_BY_FIRST_NAME:
                case FLIGHT_ATTENDANTS_ORDER_BY_FIRST_NAME: return "first_name";

                case PILOTS_ORDER_BY_LAST_NAME:
                case FLIGHT_ATTENDANTS_ORDER_BY_LAST_NAME: return "last_name";

                case PILOTS_ORDER_BY_NATIONAL_ID:
                case FLIGHT_ATTENDANTS_ORDER_BY_NATIONAL_ID: return "national_id";

                case PILOTS_ORDER_BY_DATE_OF_EMPLOYMENT:
                case FLIGHT_ATTENDANTS_ORDER_BY_DATE_OF_EMPLOYMENT: return "date_of_employment";

                case PILOTS_ORDER_BY_BIRTHDATE:
                case FLIGHT_ATTENDANTS_ORDER_BY_BIRTHDATE: return "birthdate";

            }
            return "";
        }
    }

    public ArrayList<Flight> flights = new ArrayList<>();
    public ArrayList<FlightAttendant> flightAttendants = new ArrayList<>();
    public ArrayList<Pilot> pilots = new ArrayList<>();
    public ArrayList<Plane> planes = new ArrayList<>();

    public All(Types type, boolean asc) {
        String ASC;
        String orderBy = Types.getOrderBy(type);//get the column that the data should be ordered by
        int a = Types.getOption(type);//get the wanted object
        if (asc) {
            ASC = "ASC";
        } else {
            ASC = "DESC";
        }

        Connection connect = null;
        Statement statement = null;
        ResultSet result = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:TEST.db");
            statement = connect.createStatement();
            switch (a) {
                case 1: {
                    result = statement.executeQuery("SELECT flight_serial_number" +
                            " FROM flights_info ORDER BY " + orderBy + " " + ASC);
                    while (result.next()) {
                        flights.add(new Flight(result.getString("flight_serial_number")));
                    }
                    break;
                }
                case 2: {
                    result = statement.executeQuery("SELECT national_id" +
                            " FROM flight_attendants_info ORDER BY " + orderBy + " " + ASC);
                    while (result.next()) {
                        flightAttendants.add(new FlightAttendant(result.getString("national_id")));
                    }
                    break;
                }
                case 3: {
                    result = statement.executeQuery("SELECT national_id" +
                            " FROM pilots_info ORDER BY " + orderBy + " " + ASC);
                    while (result.next()) {
                        pilots.add(new Pilot(result.getString("national_id")));
                    }
                    break;
                }
                case 4: {
                    result = statement.executeQuery("SELECT plane_serial_number" +
                            " FROM planes_info ORDER BY " + orderBy + " " + ASC);
                    while (result.next()) {
                        planes.add(new Plane(result.getString("plane_serial_number")));
                    }
                    break;
                }
            }


        } catch (Exception e) {
            JOptionPane.showMessageDialog(new JPanel(), "there was an error while getting data from database\n"
                    +e, "Error!", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                connect.close();
                statement.close();
                result.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(new JPanel(), "Error \n"+e, "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
